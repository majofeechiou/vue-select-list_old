'use strict';

var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');

var baseConfig = require('./webpack.config');

var config = Object.create(baseConfig);
var addPlugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development')
    // 'process.env.NODE_ENV': JSON.stringify('production')
  }),
  new webpack.HotModuleReplacementPlugin({
    // contentBase: "http://localhost/",
    contentBase: '/build/',
    publicPath: '/build/',
    hot: true,
    inline: true,
    quiet: false,
    noInfo: true,
    lazy: true,
    stats: {
      colors: true
    },
    proxy: {
      "*": "http://localhost:9090"
    }
  })
];

config.entry.tag.unshift( 'webpack-dev-server/client?http://localhost:8080' );
// config.entry.tag.unshift( 'client?http://localhost:8080' );

config.plugins = addPlugins.reduce( (arr, instance) => { arr.push(instance); return arr; }, 
                                    ( config.plugins || [] ) 
                                  );

module.exports = new WebpackDevServer(webpack(config), {});
