import Vue from 'vue';
import selectList from './component/selectList.vue';

(function(){
    
    new Vue({
        el: '#app',
        data: {
            showKey: 'name',
            selectedKey: 'uuid',
            selectedValue: '',
            lists: [{
                name: 'ttt測1',
                uuid: 'uuid1'
            },{
                name: 'ttt試2',
                uuid: 'uuid2'
            },{
                name: 'ttt test 3',
                uuid: 'uuid3'
            },{
                name: 'ttt測試4',
                uuid: 'uuid4'
            },{
                name: 'ttt 測試 5',
                uuid: 'uuid5'
            },{
                name: 'ttt測試 11',
                uuid: 'uuid11'
            },{
                name: 'ttt 測試21',
                uuid: 'uuid21'
            },{
                name: 'ttt花31',
                uuid: 'uuid31'
            },{
                name: 'ttt水41',
                uuid: 'uuid41'
            },{
                name: 'ttt木51',
                uuid: 'uuid51'
            }]
        },
        components: {
            'select-list': selectList
        }
    })

})();